#include "headers.h"

/* global variables used by threads which are created in main */
std::mutex mtx;
std::queue<Connection_info> q;

/*
  In main() one thread  created as a consumer(checker) before the while
   and one socket setup to accept connection from client infinitely, and as soon
   as any connection accepted, producer(adder) is called, and its work is to
   push information received from client to queue.

   @noParam
   ^return(success) : no return and continues forever
   ^return(failure) : -1
*/
int main()
{
  int *connected_sock, sock;
  pthread_t tid;

  /* This thread always checks the front of queue */
  pthread_create(&tid, NULL, checker, NULL);

  sock = server_startup("/home/strong/Git/puppeteer-shell/current_sock");

  printf("Puppeteer-shell started on socket : %s\n", "current_sock");
  while (1) {
    connected_sock = (int*) malloc(sizeof(connected_sock));
    *connected_sock = accept(sock, NULL, NULL);
    pthread_create(&tid, NULL, adder, (void*) connected_sock);
  }

  return -1;
}
