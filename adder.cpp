#include "headers.h"

/*
  This function is called whenever a new client connect to server and it read
    information sending from client and save them in a queue with time-stamp
    and if all information received correctly it echo to client with "Done"
    message otherwise "Error"

    @param(void *arg) : this parameter is filled with an address, which is the
      address of connected client socket which accept() returned.
    ^noReturn

*/
void* adder(void *arg)
{
  Connection_info ci;
  int connected_sock = *((int*) arg);
  uint32_t ip;
  uint16_t bufsz;
  char buf[READ_MAX] = {0};

  free(arg);

  if (read(connected_sock, &ip, sizeof(ip)) == -1) {
    printf("%d: %s: %s\n", __LINE__, __func__, strerror(errno));
    close(connected_sock);
  }

  else if (read(connected_sock, &bufsz, sizeof(bufsz)) == -1) {
    printf("%d: %s: %s\n", __LINE__, __func__, strerror(errno));
    close(connected_sock);
  }

  else if (read(connected_sock, &buf, bufsz) == -1) {
    printf("%d: %s: %s\n", __LINE__, __func__, strerror(errno));
    close(connected_sock);
  }

  else {
    ci.url = buf;
    ci.ip = std::to_string(ip);

    time(&ci.time_start);

    mtx.lock();
    q.push(ci);
    printf("Added to queue : %s %s\n", ci.url.data(), ci.ip.data());
    mtx.unlock();

    close(connected_sock);

  }
}
