#include "headers.h"

int server_startup(const char* sockname)
{
  int a = 1; // passing this as a parameter to setsockopt
  int sockfd;
  sockaddr_un addr;

  memset(&addr, 0, sizeof(addr));
  addr.sun_family = AF_UNIX;
  strcpy(addr.sun_path, sockname);

  if ((sockfd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
    printf("%d: %s: %s\n", __LINE__, __func__, strerror(errno));
    close(sockfd);
    return -1;
  }

  if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &a, sizeof(int)) == -1) {
    printf("%d: %s: %s\n", __LINE__, __func__, strerror(errno));
    close(sockfd);
    return -1;
  }

  if (unlink(sockname) == -1) {
    printf("%d: %s: %s\n", __LINE__, __func__, strerror(errno));
    close(sockfd);
    return -1;
  }

  if (bind(sockfd, (sockaddr*) &addr, sizeof(addr)) == -1) {
    printf("%d: %s: %s\n", __LINE__, __func__, strerror(errno));
    close(sockfd);
    return -1;
  }

  if (listen(sockfd, 10) == -1) {
    printf("%d: %s: %s\n", __LINE__, __func__, strerror(errno));
    close(sockfd);
    return -1;
  }

  return sockfd;
}

