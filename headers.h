#ifndef HEADERS_H
#define HEADERS_H

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <sys/select.h>
#include <curl/curl.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <signal.h>
#include <limits.h>
#include <sys/un.h>
#include <time.h>
#include <iomanip>
#include <chrono>
#include <ctime>
#include <map>
#include <string>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <mutex>
#include <queue>

#define MAINJS "/home/strong/Git/puppeteer-the-photographer/main.js"
#define DBPHOTO "/home/strong/DBPHOTO/"
#define EXPECTED_TIME_DIFFERENT 50

#define READ_MAX 5000

struct Connection_info;

extern std::mutex mtx;
extern std::queue<Connection_info> q;

struct Connection_info
{
  std::string ip, url;
  time_t time_start;
  Connection_info() {
    ip.clear(), url.clear();
    time_start = 0;
  }
};

int server_startup(const char* sockname);
void* checker(void *arg);
void* adder(void *arg);

#endif

