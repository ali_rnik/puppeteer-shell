#include "headers.h"
#include "myhash.h"
/*
  This function never ends, it always check front of the queue and it calculate
    the different time between the time-stamp and current time, if it is more
    than EXPECTED_TIME_DIFFERENT, puppeteer-the-photographer will run via exec.

  @noParam
  ^noReturn

*/
void* checker(void *arg)
{
  Connection_info *q_front;
  double dif;
  std::string hashed_url, command;
  std::vector<std::string> args;

  while (1) {
    args.clear();
    command.clear();

    q_front = new Connection_info;

    if (q.size() != 0) {
      mtx.lock();
      *q_front = q.front();
      mtx.unlock();

      dif = difftime(time(NULL), q_front->time_start);
      if (dif > EXPECTED_TIME_DIFFERENT) {
        mtx.lock();
        q.pop();
        mtx.unlock();
        hashed_url = myhash((char *) q_front->url.c_str());

        args.push_back("node");
        args.push_back(MAINJS);
        args.push_back(q_front->url);
        args.push_back((std::string) DBPHOTO + hashed_url);
        args.push_back("8810");
        args.push_back(q_front->ip);

        for (int i = 0; i < (int) args.size(); i++)
          command += args[i] + " ";

        system(command.data());
      }
    }

    delete q_front;
  }
}
